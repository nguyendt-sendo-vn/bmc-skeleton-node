const Joi = require("joi");

module.exports = {
    path: Joi.string().required()
};
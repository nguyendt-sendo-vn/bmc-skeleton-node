const Joi = require("joi");

module.exports = Joi.array().items({
    instance: Joi.string().required(),
    host: Joi.string().required(),
    port: Joi.number().integer().required()
});
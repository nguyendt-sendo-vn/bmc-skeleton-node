const Joi = require("joi");

module.exports = {
    console: Joi.boolean().optional(),
    service_name: Joi.string().token().required(),
};
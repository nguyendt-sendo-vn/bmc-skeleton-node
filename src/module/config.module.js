const fs = require("fs");
const path = require("path");
const utils = require("lodash");
const yaml = require("js-yaml");

/**
 * @class ConfigModule
 */
class ConfigModule {

    /**
     * @section General Properties
     */

    /**
     * @param {object} opts
     */
    constructor(opts = {}) {

        this.instances = {};

        this.opts = opts;

    }

    /**
     * @returns {Promise}
     */
    init() {

        const self = this;

        return new Promise(function (resolve) {

            // Step #1
            // Loading from configuration files.

            const files = fs.readdirSync(self.opts.path);

            for (let file of files) {

                if (file.endsWith(".js") === false)

                    continue;

                let instance = path.parse(file).name;

                self.instances[instance] = require(self.opts.path + "/" + instance);

            }

            // Step #2
            // Loading from .yml file.

            let configPath = process.cwd() + "/.yaml";

            if (fs.existsSync(configPath)) {

                let configContent = fs.readFileSync(configPath, "utf8");

                configContent = yaml.safeLoad(configContent);

                self.instances = utils.merge(self.instances, configContent);

            }

            // Step #3
            // Finalize and return

            resolve();

        });

    }

    /**
     * @param {string} record
     * @param {*|null} defaultValue
     * @returns {*|null}
     */
    get(record, defaultValue = null) {

        if (record === "*")

            return this.instances;

        return utils.get(this.instances, record) || defaultValue;

    }

}

module.exports = ConfigModule;
const utils = require("lodash");

const RedisClient = require("ioredis-custom");

/**
 * @class RedisModule
 */
class RedisModule {

    /**
     * @param {object} opts
     */
    constructor(opts = {}) {

        this.instances = {};

        this.opts = opts;

    }

    /**
     * @returns {Promise}
     */
    init() {

        const self = this;

        return new Promise(function (resolve, reject) {

            let jobObjects = [];

            for (let opt of self.opts)

                jobObjects.push(self.initInstance(opt));

            Promise.all(jobObjects).then(function () {

                resolve();

            }).catch(function (E) {

                reject(E);

            });

        });

    }

    /**
     * @param {object} opt
     * @returns {Promise}
     */
    initInstance(opt) {

        const self = this;

        return new Promise(async function (resolve) {

            let instanceObject = new RedisClient({
                host: opt["host"],
                port: opt["port"]
            });

            let response = await instanceObject.ping();

            if (response !== "PONG")

                throw new Error(`Can't ping Redis instance at ${opt["host"]}:${opt["port"]}.`);

            utils.set(self.instances, opt["instance"], instanceObject);

            resolve();

        });

    }

    /**
     * @param {null|string} instance
     * @returns {RedisClient}
     */
    getInstance(instance = null) {

        return instance === null
            ? this.instances[Object.keys(this.instances)[0]]
            : this.instances[instance];

    }

    /**
     * @returns {boolean}
     */
    closeInstances() {

        for (let index of Object.keys(this.instances))

            this.instances[index].disconnect();

        return true;

    }

    /**
     * @section Specified Properties
     */

}

module.exports = RedisModule;
const fs = require("fs");
const joi = require("joi");
const utils = require("lodash");

/**
 * @class ValidateModule
 */
class ValidateModule {

    /**
     * @section General Properties
     */

    /**
     * @param {object} opts
     */
    constructor(opts = {}) {

        this.instances = {};

        this.opts = opts;

    }

    /**
     * @returns {Promise}
     */
    init() {

        const self = this;

        return new Promise(function (resolve) {

            const files = fs.readdirSync(self.opts.path);

            for (let file of files) {

                if (file.endsWith(".schema.js") === false)

                    continue;

                let instance = file.replace(".schema.js", "");

                self.instances[instance] = require(self.opts.path + "/" + file);

            }

            resolve();

        });

    }

    /**
     * @section Specified Properties
     */

    /**
     * @param {string} schemaIndicator
     * @param {object} inputObject
     * @returns {boolean|object}
     */
    validate(schemaIndicator, inputObject) {

        // Step #1
        // Collecting the schemaObject based on the schemaIndicator

        const schemaObject = utils.get(this.instances, schemaIndicator);

        if (schemaObject === undefined)

            throw new Error(`Schema "${schemaIndicator}" isn't defined.`);

        // Step #2
        // Implement the Validating

        let resultObject = joi.validate(inputObject, schemaObject);

        if (resultObject.error === null)

            return true;

        // Step #3
        // Generate the list of error

        let errorObjects = [];

        resultObject.error.details.forEach(function (detail) {

            errorObjects.push(detail.message);

        });

        return errorObjects;

    }

}

module.exports = ValidateModule;
const chalk = require("chalk");
const moment = require("moment");
const os = require("os");
const stackTrace = require("stack-trace");
const utils = require("lodash");

/**
 * @class LogModule
 */
class LogModule {

    /**
     * @section General Properties
     */

    /**
     * @param {object} opts
     */
    constructor(opts = {}) {

        this.opts = opts;

    }

    /**
     * @returns {Promise}
     */
    init() {

        const self = this;

        return new Promise(function (resolve) {

            self["EOL"] = utils.get(os, "EOL");

            resolve();

        });

    }

    /**
     * @section Specified Properties
     */

    /**
     * @param {string} message
     * @param {*} metaData
     */
    info(message, metaData = {}) {

        if (this.opts.console) {

            console.log(moment().format(), chalk.bold.blue("INFO"), message);

            if (metaData)

                console.log(metaData);

            return;

        }

        this.transport("INFO", message, metaData);

    }

    /**
     * @param {string} message
     * @param {*} metaData
     */
    warn(message, metaData = {}) {

        if (this.opts.console) {

            console.log(moment().format(), chalk.bold.yellow("WARNING"), message);

            if (metaData)

                console.log(metaData);

            return;

        }

        this.transport("WARN", message, metaData);

    }

    /**
     * @param {object} message
     * @param {*} metaData
     */
    error(message, metaData = {}) {

        // Step #1
        // Handle the case of debug

        if (this.opts.console) {

            console.log(moment().format(), chalk.bold.red("ERROR"), message);

            if (metaData)

                console.log(metaData);

            return;

        }

        // Step #2
        // Handle the case of error stack is provided

        if (
            utils.get(metaData, "error")
            && utils.get(metaData, "error.message")
            && utils.get(metaData, "error.stack")
        ) {
            metaData["errorStack"] = utils.get(metaData, "error.stack");
            metaData["error"] = utils.get(metaData, "error.message");
        }

        this.transport("ERROR", message, metaData);

    }

    /**
     * @param {string} level
     * @param {string} message
     * @param {*} metaData
     */
    transport(level, message, metaData) {

        // Step #1
        // Define the "function_name" value.

        const traceObject = stackTrace.get()[3];

        let functionName = "";
        functionName += traceObject.getFileName().replace(process.cwd() + "/", "");
        functionName += "::";
        functionName += traceObject.getFunctionName() ? traceObject.getFunctionName() : "main";

        // Step #2
        // Prepare the output

        const output = {};

        output["@timestamp"] = moment().format();

        output["level"] = level;

        output["hostname"] = utils.get(process.env, "HOSTNAME", "undefined");
        output["service_name"] = utils.get(this.opts, "service_name", "undefined");
        output["function_name"] = functionName;

        output["message"] = message;
        output["metaData"] = JSON.stringify(metaData);

        // Step #3
        // Write the output.

        process.stdout.write(JSON.stringify(output) + this.EOL);

    }

}

module.exports = LogModule;
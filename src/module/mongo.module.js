const utils = require("lodash");

/**
 * @class MongoModule
 */
class MongoModule {

    /**
     * @section General Properties
     */

    /**
     * @param {object} opts
     * @param {object} parent
     */
    constructor(opts = {}, parent) {

        this.instances = {};

        this.opts = opts;
        this.parent = parent;

    }

    /**
     * @returns {Promise}
     */
    init() {

        const self = this;

        return new Promise(function (resolve, reject) {

            let jobObjects = [];

            for (let opt of self.opts) {

                jobObjects.push(self.initInstance(opt));

            }

            Promise.all(jobObjects).then(function () {

                resolve();

            }).catch(function (E) {

                reject(E);

            });

        });

    }

    /**
     * @param {object} opt
     * @returns {Promise}
     */
    initInstance(opt) {

        const self = this;

        return new Promise(function (resolve, reject) {

            // Step #1
            // Prepare for the connection

            const MongoClient = require("mongodb").MongoClient;

            let mongoUri = "mongodb://";

            mongoUri += opt.username ? opt.username + ":" : "";
            mongoUri += opt.password ? opt.password + "@" : "";
            mongoUri += opt.host + ":";
            mongoUri += opt.port + "/";
            mongoUri += opt.name;

            // Step #2
            // Connect and keep the connection

            MongoClient.connect(mongoUri).then(function (instance) {

                instance.on("close", onClose);

                utils.set(self.instances, opt["instance"], instance);

                resolve();

            }).catch(function (error) {

                reject(error);

            });

        });

    }

    /**
     * @param {null|string} instance
     * @returns {MongoClient}
     */
    getInstance(instance = null) {

        return instance === null
            ? this.instances[Object.keys(this.instances)[0]]
            : this.instances[instance];

    }

    /**
     * @returns {boolean}
     */
    closeInstances() {

        for (let index of Object.keys(this.instances)) {

            this.instances[index].removeListener("close", onClose);

            this.instances[index].close();

        }

        return true;

    }

    /**
     * @section Specified Properties
     */

    /**
     * @param {string} collection
     * @param {null|string} instance
     * @returns {Collection}
     */
    getCollection(collection, instance = null) {

        return instance === null
            ? this.instances[Object.keys(this.instances)[0]].collection(collection)
            : this.instances[instance].collection(collection);

    }

}

module.exports = MongoModule;

/**
 * onClose()
 */
function onClose() {

    throw new Error("Mongo instance is closed.");

}
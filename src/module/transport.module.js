const fetch = require("fetch");
const utils = require("lodash");

/**
 * @class TransportModule
 */
class TransportModule {

    /**
     * @section General Properties
     */

    /**
     * @param {object} opts
     */
    constructor(opts = {}) {

        this.opts = opts;

    }

    /**
     * @returns {Promise}
     */
    init() {

        const self = this;

        return new Promise(function (resolve) {

            resolve();

        });

    }

    /**
     * @param {string} method
     * @param {string} url
     * @param {*|null} payload
     * @param {object} headers
     * @returns {Promise<Object>}
     */
    transport(method, url, payload = null, headers = {}) {

        let output = {
            error: false,
            body: null,
            status: 0
        };

        return new Promise(function (resolve) {

            let opts = {};

            opts["method"] = method;

            // Step #1
            // Working with the Headers

            if (payload !== null && typeof payload === "object" && utils.get(headers, "Content-Type") === undefined)

                utils.set(headers, "Content-Type", "application/json");

            opts["headers"] = headers;

            // Step #2
            // Working with the payload content

            if (payload !== null)

                opts["payload"] = typeof payload === "object" ? JSON.stringify(payload) : payload;

            // Step #3
            // Sending request

            fetch.fetchUrl(url, opts, function (error, meta, body) {

                if (error) {

                    output.error = true;
                    output.body = error.message;

                } else {

                    output.body = body.toString();
                    output.status = meta.status;

                    try {

                        output.body = JSON.parse(output.body);

                    } catch (E) {

                        // Nothing to concern about...

                    }

                }

                resolve(output);

            });

        });

    }

}

module.exports = TransportModule;
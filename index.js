const joi = require("joi");
const utils = require("lodash");

/**
 * @class Skeleton
 */
class Skeleton {

    /**
     * @param {object} opts
     */
    constructor(opts = {}) {

        this.modules = {};

        this.opts = opts;

    }

    /**
     * @param {string} module
     * @returns {Promise}
     */
    register(module) {

        const self = this;

        // Step #1
        // Validate the required options

        const opts = utils.get(self.opts, module);

        if (opts === undefined)

            throw new Error(`Module "${module}" configuration not found.`);

        // Step #2
        // Initialize the required module

        return new Promise(function (resolve, reject) {

            // Step #2.1
            // Define the required module

            let ModuleClass = null;
            let ModuleSchema = null;

            switch (module) {

                case "config":

                    ModuleClass = require("./src/module/config.module");
                    ModuleSchema = require("./src/schema/config.schema");

                    break;

                case "log":

                    ModuleClass = require("./src/module/log.module");
                    ModuleSchema = require("./src/schema/log.schema");

                    break;

                case "mongo":

                    ModuleClass = require("./src/module/mongo.module");
                    ModuleSchema = require("./src/schema/mongo.schema");

                    break;

                case "redis":

                    ModuleClass = require("./src/module/redis.module");
                    ModuleSchema = require("./src/schema/redis.schema");

                    break;

                case "transport":

                    ModuleClass = require("./src/module/transport.module");

                    break;

                case "validate":

                    ModuleClass = require("./src/module/validate.module");
                    ModuleSchema = require("./src/schema/validate.schema");

                    break;

                default:

                    reject(new Error(`Module "${module}" not found.`));

                    return false;

            }

            // Step #2.2
            // Validate the Module options

            if (ModuleSchema !== null) {

                let validateResult = joi.validate(opts, ModuleSchema);

                if (validateResult.error !== null) {

                    let errors = [];

                    validateResult.error.details.forEach(function (detail) {

                        errors.push(detail.message);

                    });

                    reject(new Error(`Invalid options for "${ module }" module: ${ errors.join("; ") }.`));

                    return false;

                }

            }

            // Step #2.3
            // Initialize and resolve the required module.

            const moduleObject = new ModuleClass(opts, self);

            moduleObject.init().then(function () {

                utils.set(self.modules, module, moduleObject);

                resolve();

            });

        });

    }

    /**
     * @returns {boolean}
     */
    close() {

        for (let index of Object.keys(this.modules))

            if (typeof this.modules[index]["closeInstances"] === "function")

                this.modules[index].closeInstances();

        return true;

    }

    /**
     * @section General
     */

    /**
     * @param {null|string} instance
     * @returns {MongoClient}
     */
    getMongo(instance = null) {

        if (utils.get(this.modules, "mongo") === undefined)

            throw new Error(`Module "mongo" is not registered.`);

        return this["modules"]["mongo"].getInstance(instance);

    }

    /**
     * @param {null|string} instance
     * @returns {RedisClient}
     */
    getRedis(instance = null) {

        if (utils.get(this.modules, "redis") === undefined)

            throw new Error(`Module "redis" is not registered.`);

        return this["modules"]["redis"].getInstance(instance);

    }

    /**
     * @section Config Module
     */

    /**
     * @param {string} record
     * @param {*|null} defaultValue
     * @returns {*|null}
     */
    config(record, defaultValue = null) {

        if (utils.get(this.modules, "config") === undefined)

            throw new Error(`Module "config" is not registered.`);

        return this["modules"]["config"]["get"](record, defaultValue);

    }

    /**
     * @section Log Module
     */

    /**
     * @param {string} message
     * @param {*} metaData
     */
    info(message, metaData = {}) {

        if (utils.get(this.modules, "log") === undefined)

            throw new Error(`Module "log" is not registered.`);

        this["modules"]["log"]["info"](message, metaData);

    }

    /**
     * @param {string} message
     * @param {*} metaData
     */
    warn(message, metaData = {}) {

        if (utils.get(this.modules, "log") === undefined)

            throw new Error(`Module "log" is not registered.`);

        this["modules"]["log"]["warn"](message, metaData);

    }

    /**
     * @param {string} message
     * @param {*} metaData
     */
    error(message, metaData = {}) {

        if (utils.get(this.modules, "log") === undefined)

            throw new Error(`Module "log" is not registered.`);

        this["modules"]["log"]["error"](message, metaData);

    }

    /**
     * @section MongoDB Module
     */

    /**
     * @param {string} collection
     * @param {null|string} instance
     * @returns {Collection}
     */
    collection(collection, instance = null) {

        if (utils.get(this.modules, "mongo") === undefined)

            throw new Error(`Module "mongo" is not registered.`);

        return this["modules"]["mongo"].getCollection(collection, instance);

    }

    /**
     * @section Transport Module
     */

    /**
     * @param {string} url
     * @param {object} headers
     * @returns {Promise.<*>}
     */
    get(url, headers = {}) {

        if (utils.get(this.modules, "transport") === undefined)

            throw new Error(`Module "transport" is not registered.`);

        return this["modules"]["transport"].transport("GET", url, null, headers);

    }

    /**
     * @param {string} url
     * @param {object} body
     * @param {object} headers
     */
    post(url, body, headers = {}) {

        if (utils.get(this.modules, "transport") === undefined)

            throw new Error(`Module "transport" is not registered.`);

        return this["modules"]["transport"].transport("POST", url, body, headers);

    }

    /**
     * @section Validate Module
     */

    /**
     * @param {string} schemaIndicator
     * @param {object} inputObject
     * @returns {boolean|object}
     */
    validate(schemaIndicator, inputObject)  {

        if (utils.get(this.modules, "validate") === undefined)

            throw new Error(`Module "validate" is not registered.`);

        return this["modules"]["validate"].validate(schemaIndicator, inputObject);

    }

}

module.exports = Skeleton;